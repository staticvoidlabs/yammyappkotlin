package com.staticvoidlabs.yammy

class Dish (
    var id: String,
    var title: String,
    var season: String,
    var image_bytes: String,
    var image_url: String,
    var shown: Boolean
)
{}