package com.staticvoidlabs.yammy

import YammyLibs.YammyLibs.getYammyDishesFromUrl
import YammyLibs.YammyLibs.sendDeviceTokenToYammyServices
import android.content.ActivityNotFoundException
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import java.util.*


class MainActivity : AppCompatActivity() {

    private lateinit var dishesRepo: YammyDishes
    private lateinit var selectedDish: Dish
    private var selectedSeason: String = "A"
    private var token: String = "yyyyy"
    private var deviceId: String = "xxxxx"

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Get current FCM device token.
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                //Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token.
            val tmpToken = task.result

            if (tmpToken != null) {
                token = tmpToken
            }
            Log.d(ContentValues.TAG, "FCM: Refreshed token: $token")

            // Get/set unique device id.
            val tmpSharedPref = this?.getPreferences(Context.MODE_PRIVATE)
            val tmpSharedPrefDeviceId = tmpSharedPref.getString("yammy_device_id", "n/a")

            if (tmpSharedPrefDeviceId == "n/a"){

                deviceId = UUID.randomUUID().toString()

                with (tmpSharedPref.edit()) {
                    putString("yammy_device_id", deviceId)
                    apply()
                }

            }
            else if (tmpSharedPrefDeviceId != null) {
                deviceId = tmpSharedPrefDeviceId
            }
            else {
                deviceId = "xxxxx"
            }

            Log.d(ContentValues.TAG, "FCM: Unique device ID: $deviceId")

            UpdateFcmToken()
        })


        // Relative Layout itself.
        val relativeLayout = findViewById<RelativeLayout>(R.id.relativeLayout);
        relativeLayout.setBackgroundColor(Color.parseColor("#2E2E2E"));

        // TextView "Title".
        val titleTextView = findViewById<TextView>(R.id.titleDishes);
        titleTextView.setTextColor(Color.parseColor("#808080"));

        // Button "Season".
        val buttonSeasonSwitcher = findViewById<Button>(R.id.buttonSeasonSwitcher);
        //buttonSeasonSwitcher.setTextColor(Color.WHITE)
        //buttonSeasonSwitcher.setBackgroundColor(Color.DKGRAY)
        buttonSeasonSwitcher.setOnClickListener {
            if (buttonSeasonSwitcher.text == "S") {
                buttonSeasonSwitcher.text = "W"
                selectedSeason = "W"
            }
            else if (buttonSeasonSwitcher.text == "W") {
                buttonSeasonSwitcher.text = "A"
                selectedSeason = "A"
            }
            else {
                buttonSeasonSwitcher.text = "S"
                selectedSeason = "S"
            }

            InitDishesRepo()
        }

        // Button "Next".
        val buttonNext = findViewById<Button>(R.id.buttonNext);
        buttonNext.setTextColor(Color.WHITE)
        buttonNext.setOnClickListener {
            showNextDish()
        }

        // Button "Send email".
        val buttonSendEmail = findViewById<Button>(R.id.buttonSendEmail);
        buttonSendEmail.setOnClickListener {
            SendEmail()
        }

        // Init dishes repo and show first dish.
        InitDishesRepo()

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun InitDishesRepo() {

        val gson = Gson()
        lateinit var tmpDishes: YammyDishes

        // Get an decode JSON from web service.
        val jsonReturnValue: String = getYammyDishesFromUrl("http://www.staticvoidlabs.com/yammy/getdishes.php")
        tmpDishes = gson.fromJson(jsonReturnValue, YammyDishes::class.java)

        // Filter by selected season.
        when (selectedSeason) {
            "S" -> tmpDishes.Dishes = tmpDishes.Dishes.filter { it.season == "S" }
            "W" -> tmpDishes.Dishes = tmpDishes.Dishes.filter { it.season == "W" }
            else -> {
            }
        }

        // Randomize dishes list.
        tmpDishes.Dishes = tmpDishes.Dishes.shuffled()

        this.dishesRepo = tmpDishes

        showNextDish()
    }

    fun UpdateFcmToken() {

        val tmpApiParam: String = token + "_____" + deviceId
        Log.d(ContentValues.TAG, "FCM: Prepared APi parameter: $tmpApiParam")

        // Send updated device token to YammyServices.
        // Flag "isDebug" switches destination from "http://www.fatseven.com/fcm/DEVICETOKEN" to "http://localhost:9100/fcm/DEVICETOKEN"
        sendDeviceTokenToYammyServices(tmpApiParam, false)
    }

    fun showNextDish() {

        for (tmpDish in dishesRepo.Dishes) {

            if (!tmpDish.shown){

                // Set title.
                val tmpTextView = findViewById<TextView>(R.id.titleDishes)
                tmpTextView.text = tmpDish.title
                tmpDish.shown = true

                // Set image.
                val tmpImageView = findViewById<ImageView>(R.id.imageViewDishes)

                if (tmpDish.image_url != "") {
                    Picasso.get().load(tmpDish.image_url).into(tmpImageView)
                }
                else {
                    tmpImageView.setImageResource(0)
                }

                selectedDish = tmpDish

                break
            }

            //val buttonNext = findViewById<Button>(R.id.button_next);
            //buttonNext.text = "Ende..."
        }

    }

    fun SendEmail() {

        val i = Intent(Intent.ACTION_SEND)
        i.type = "message/rfc822"
        i.putExtra(Intent.EXTRA_EMAIL, arrayOf("yammy@algrande.net"))
        i.putExtra(Intent.EXTRA_SUBJECT, "Leckerschmecker")
        i.putExtra(Intent.EXTRA_TEXT, selectedDish.title)
        try {
            startActivity(Intent.createChooser(i, "Abendessen per Email bestellen."))
        } catch (ex: ActivityNotFoundException) {
            Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show()
        }

    }

}